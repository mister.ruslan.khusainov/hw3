// Fill out your copyright notice in the Description page of Project Settings.


#include "../Public/PickupQuestion.h"

#include "MyProject3/Public/ILessonCharacter.h"

void APickupQuestion::OnPickedUp(AActor* OtherActor)
{
	Super::OnPickedUp(OtherActor);
	ILessonCharacter::Execute_OnReachQuestion(OtherActor);
	Destroy();
}
