// Fill out your copyright notice in the Description page of Project Settings.


#include "MyProject3/Public/PickUpActor.h"
#include "NiagaraFunctionLibrary.h"
#include "Sound/SoundCue.h"

#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MyProject3/Public/ILessonCharacter.h"

// Sets default values
APickUpActor::APickUpActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RooT"));
	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphere"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	OverlapSphere->SetupAttachment(RootComponent);
	OverlapSphere->SetSphereRadius(100);
	OverlapSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapSphere->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapSphere->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	Mesh->SetupAttachment(OverlapSphere);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void APickUpActor::BeginPlay()
{
	Super::BeginPlay();

	OverlapSphere->OnComponentBeginOverlap.AddDynamic(this, &APickUpActor::onSphereOverlap);
}

void APickUpActor::Destroyed()
{
	Super::Destroyed();
	if (PickupEmitter)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, PickupEmitter, GetActorLocation(), GetActorRotation());
	}
	if (PickupSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(this, PickupSound, GetActorLocation());
	}
}


// Called every frame
void APickUpActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Mesh->AddRelativeRotation(FRotator(0.f, BaseRotationRate * DeltaTime, 0.f));
}

void APickUpActor::onSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
                                   const FHitResult& SweepResult)
{
	if (OtherActor->Implements<ULessonCharacter>())
	{
		OnPickedUp(OtherActor);
	}
	onSphereOverlapped();
	onItemPickuped.Broadcast();
}

void APickUpActor::OnPickedUp(AActor* OtherActor)
{
}
