// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUpActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FItemPickuped);
UCLASS()
class MYPROJECT3_API APickUpActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickUpActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Destroyed() override;
	virtual void Tick(float DeltaTime) override;

public:

	UPROPERTY(EditAnywhere)
	class USphereComponent* OverlapSphere;

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* Mesh;
	
	UFUNCTION()
	void onSphereOverlap(class UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	                     UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
	                     const FHitResult&  SweepResult);

	UFUNCTION(BlueprintImplementableEvent)
	void onSphereOverlapped();

	UPROPERTY(BlueprintAssignable)
	FItemPickuped onItemPickuped;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	class UNiagaraSystem* PickupEmitter;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	class USoundCue* PickupSound;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float BaseRotationRate = 45.f;

	virtual void OnPickedUp(AActor* OtherActor);
	
};
