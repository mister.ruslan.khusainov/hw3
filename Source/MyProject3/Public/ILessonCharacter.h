#pragma once
#include "CoreMinimal.h"
#include "ILessonCharacter.generated.h"

UINTERFACE()
class ULessonCharacter : public UInterface
{
	GENERATED_BODY()
};

class MYPROJECT3_API ILessonCharacter
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnReachQuestion();
};

