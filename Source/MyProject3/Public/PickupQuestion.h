// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpActor.h"
#include "UObject/Object.h"
#include "PickupQuestion.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT3_API APickupQuestion : public APickUpActor
{
	GENERATED_BODY()

public:
	virtual void OnPickedUp(AActor* OtherActor) override;
};

